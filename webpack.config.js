'use strict';

const webpack = require('webpack');
const path = require('path');
var nodeExternals = require('webpack-node-externals');

var argv = require('minimist')(process.argv.slice(2));

var fileExt = argv['optimize-minimize'] ? ".min.js" : ".js";

module.exports = function (env) {
    return {
        entry: {
            form: './src/formView.js',
            selectBox: './src/inputFields/selectbox/selectView.js',
            fileUploadBox: './src/inputFields/fileUpload/fileUploadView.js',
            dateTimePicker: './src/inputFields/dateTimePicker/dateTimePickerView.js',
            checkBox: './src/inputFields/checkbox/checkboxView.js'
        },
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env', "es2015"]
                        }
                    }
                }
            ],
            noParse: [
                /node_modules\/clipboard\/dist\/clipboard.min.js/
            ]
        },
        target: 'node',
        externals: [nodeExternals()],
        output: {
            path: path.resolve(__dirname, 'lib'),
            filename: "[name]/[name].view" + fileExt
        },
        plugins: [
//            new UglifyJSPlugin()
        ],
        resolve: {
            descriptionFiles: ["package.json"],
            modules: [
                "node_modules",
            ],
            alias: {
                'underscore': __dirname + '/node_modules/lodash/',
                'lodash': __dirname + '/node_modules/lodash/',
                'backbone': __dirname + '/node_modules/backbone/backbone.js',
            }
        }
    }
};

