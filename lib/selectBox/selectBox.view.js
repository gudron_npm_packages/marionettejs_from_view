(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("jquery.nicescroll"), require("select2"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "jquery.nicescroll", "select2"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("jquery.nicescroll"), require("select2")) : factory(root["backbone.marionette"], root["jquery.nicescroll"], root["select2"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _jquery = __webpack_require__(4);

var _jquery2 = _interopRequireDefault(_jquery);

var _select = __webpack_require__(5);

var _select2 = _interopRequireDefault(_select);

var _optionView = __webpack_require__(6);

var _optionView2 = _interopRequireDefault(_optionView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// SelectView
// ---------

var customScrollOptions = {
    background: '#e0e9f3',
    autohidemode: false,
    cursorcolor: '#cad1d8',
    cursorborder: '#cad1d8',
    cursorwidth: "4px",
    spacebarenabled: false,
    horizrailenabled: false,
    sensitiverail: false,
    /**
     * MutationObserver should be disabled
     * 'cause the scrollbar begins to flicker
     * when moving from one option to another
     * inside the dropdown
     **/
    disablemutationobserver: true
};

var SelectView = _backbone2.default.CollectionView.extend({
    tagName: 'select',
    className: 'select2-select',
    childView: _optionView2.default,
    optionText: false,
    events: {
        'select2:open': function select2Open(evt) {
            // If there's only the default option, show the preloader
            if (evt.target.childElementCount <= 1) {
                $('.select2-results').append(this._compiledPreloaderTemplate);
                return;
            }

            $('.select2-results__options').niceScroll(customScrollOptions);
        },
        'select2:closing': function select2Closing() {
            $('.select2-results').children('.preloader-wrapper').remove();
            $('.select2-results__options').getNiceScroll().remove();
        },
        'change': function change(event) {

            var id = this.$el.select2('data')[0]['id'];
            var model = this.collection._byId[id];
            this.trigger('change', event, model);
            this.$el.trigger('change.select2', event, model);
        }
    },
    /*    triggers: {
            "change": "change"
        },*/
    childViewTemplate: false,
    childViewNameAttribute: 't_name',
    selectOptions: [],
    select2Options: {
        language: {
            noResults: function noResults() {
                return App.t('global.select_view_no_results');
            }
        },
        theme: 'gmt',
        placeholder: '',
        width: '100%',
        dropdownAutoWidth: true
    },
    current_item_id: false,
    preloaderTemplate: false,
    _compiledPreloaderTemplate: false,
    initialize: function initialize() {
        this.childViewNameAttribute = this.getOption('childViewNameAttribute');

        if (this.getOption('optionText')) {
            this.optionText = this.getOption('optionText');
        }

        if (this.getOption('current_item_id')) {
            this.current_item_id = this.getOption('current_item_id');
        }

        this.preloaderTemplate = this.getOption('preloaderTemplate');
        this._compiledPreloaderTemplate = this.preloaderTemplate({
            hasWrapper: true,
            title: App.t('global.preloader_title')
        });
    },
    onShow: function onShow() {
        this.$el.prepend($('<option>').attr('selected', true));
        this.$el.select2(this.select2Options);
    },
    buildChildView: function buildChildView(child, ChildViewClass, childViewOptions) {
        childViewOptions = childViewOptions || {};

        var model_id = null;
        if (typeof child.getEntityId !== 'undefined') {
            model_id = child.getEntityId();
        } else {
            model_id = child.get('id');
        }
        // build the final list of options for the childView class
        var attributes = {
            value: model_id,
            'data-code': model_id
        };

        if (childViewOptions.hasOwnProperty('attributes')) {
            _.extend(attributes, childViewOptions.attributes);
        }

        if (this.current_item_id == model_id) {
            _.extend(attributes, {
                selected: true
            });
        }

        // create the child view instance
        var optionViewProperties = {};
        _.extend(optionViewProperties, childViewOptions, {
            attributes: attributes
        });

        if (this.childViewTemplate) {
            _.extend(optionViewProperties, {
                template: this.childViewTemplate
            });
        }

        var view = new _optionView2.default(optionViewProperties);
        this.selectOptions.push(view);

        var text = child.get(this.childViewNameAttribute);
        if (this.optionText) {
            if (_.isFunction(this.optionText)) {
                var optionText = _.bind(this.optionText, child);
                text = optionText();
            } else {
                text = child.get(this.optionText);
            }
        } else if (typeof child.optionText === 'function') {
            text = child.optionText();
        }
        view.$el.text(text);

        // return it
        return view;
    },
    onRenderCollection: function onRenderCollection(view) {
        // When select options arrived and rendered,
        // reload an opened select dropdown (if select2 is initialized)
        if (view.$el.data('select2') && view.$el.select2('isOpen')) {
            view.$el.select2('close').select2('open');
        }
    },
    onBeforeDestroy: function onBeforeDestroy() {
        _.each(this.selectOptions, function (optionView, index) {
            optionView.destroy();
        });

        this.$el.select2('val', '');
        this.$el.select2('enable', false);
        this.$el.select2('destroy');

        delete this._compiledPreloaderTemplate;
    },
    setCurrentItem: function setCurrentItem(item) {
        var that = this;
        this.current_item_id = item;
        _.each(this.selectOptions, function (optionView, index) {
            if (optionView.attributes.value == item) {
                optionView.setSelected();
                that.$el.change();
            }
        });
    }
});

exports.default = SelectView;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("jquery.nicescroll");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("select2");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var OptionView = _backbone2.default.ItemView.extend({
    tagName: 'option',
    template: false,
    setSelected: function setSelected() {
        this.$el.prop('selected', true);
    },
    setUnselected: function setUnselected() {
        this.$el.prop('selected', false);
        this.$el.removeProperty('selected');
    }
}); // OptionView
// ---------

exports.default = OptionView;

/***/ })
/******/ ]);
});
//# sourceMappingURL=selectBox.view.js.map