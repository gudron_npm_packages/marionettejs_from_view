(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("qtip2"), require("resumablejs"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "qtip2", "resumablejs"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("qtip2"), require("resumablejs")) : factory(root["backbone.marionette"], root["qtip2"], root["resumablejs"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_8__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("qtip2");

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _resumablejs = __webpack_require__(8);

var _resumablejs2 = _interopRequireDefault(_resumablejs);

var _qtip = __webpack_require__(1);

var _qtip2 = _interopRequireDefault(_qtip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FileUploadView = _backbone2.default.ItemView.extend({
    ui: {
        title: '.file-upload__title',
        preloader: '.file-upload__preloader',
        buttonWrapper: '.btn'
    },
    input: false,
    $input: false,
    r: false,
    maxFileSize: function maxFileSize() {
        return 5 * 1024 * 1024;
    },
    acceptableFileTypes: ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'image/jpeg', 'image/png', 'image/tiff', 'image/gif'],
    errors: function errors() {
        return {
            invalidFileSize: App.t('global.invalid_file_size'),
            invalidFileType: App.t('global.invalid_file_type')
        };
    },
    preloaderTemplate: false,
    template: false,
    initialize: function initialize() {
        var that = this;

        this.r = new _resumablejs2.default({
            maxFiles: 1,
            chunkSize: _.result(this, 'maxFileSize'),
            testChunks: false,
            simultaneousUploads: 1,
            prioritizeFirstAndLastChunk: true,
            singleFile: true,
            headers: { 'Authorization': 'Bearer ' + App.apiToken.get('access_token') }
        });

        this.r.on('filesAdded', function (files, arraySkipped) {
            return that._onFileChange(files);
        });

        this.r.on('fileSuccess', function (file, message) {
            return that._onFileUploadSuccess(file, message);
        });

        this.preloaderTemplate = PreloaderTemplate({
            hasWrapper: true,
            title: App.t('global.preloader_title')
        });
    },
    onRender: function onRender() {
        this.$input = this.$el.find('.js-upload-file-input');
        this.input = this.$input[0];

        this.$input.prop('id', this.getOption('id'));

        this.ui.preloader.append(this.preloaderTemplate);
    },
    onShow: function onShow() {
        var that = this;

        that.r.assignBrowse(that.input, false);
    },
    onBeforeDestroy: function onBeforeDestroy() {
        delete this.input;
        delete this.$input;
        delete this.r;
        this.$input = this.input = false;
    },
    _onFileChange: function _onFileChange(files) {
        var that = this;

        that._setInProgressState();

        var file = _.first(files);
        var errors = [];
        var errorMessages = _.result(this, 'errors');
        var hasError = false;

        if (!this._isValidFileSize(file)) {
            hasError = true;
            errors.push(errorMessages.invalidFileSize);
        }

        if (!this._isValidFileType(file)) {
            hasError = true;
            errors.push(errorMessages.invalidFileType);
        }

        if (!hasError) {
            this.trigger('file:validation:success', this.r, file);
            that.onFileReadSuccess(file);
        } else {
            that.onFileValidationError(file, errors);
            this.trigger('file:validation:error', this.r, file, errors);
        }
    },
    _onFileUploadSuccess: function _onFileUploadSuccess(file, message) {
        this.trigger('file:upload:success', file, message);
        this.reset();
    },
    onFileReadSuccess: function onFileReadSuccess(file) {
        this._setCompleteState();

        this.ui.title.text(file.name);

        this.trigger('file:read:success', this.r, file);
    },
    onFileValidationError: function onFileValidationError(file, errors) {
        this._setCompleteState();
        this.ui.title.text(App.t('global.file_button_has_error') + _.first(errors));
    },
    reset: function reset() {
        this.ui.title.text(App.t('global.file_button_choose_file'));
        this._setCompleteState();
    },
    disable: function disable() {
        this.$input.prop('disabled', true);
        this.ui.buttonWrapper.addClass('btn--ghost-gray');
        this.ui.buttonWrapper.removeClass('btn--ghost-wild-blue');
    },
    enable: function enable() {
        this.$input.prop('disabled', false);
        this.ui.buttonWrapper.removeClass('btn--ghost-gray');
        this.ui.buttonWrapper.addClass('btn--ghost-wild-blue');
    },
    _isValidFileSize: function _isValidFileSize(rfile) {
        return rfile.size < _.result(this, 'maxFileSize');
    },
    _isValidFileType: function _isValidFileType(rfile) {
        return _.contains(_.result(this, 'acceptableFileTypes'), rfile.file.type);
    },
    _setInProgressState: function _setInProgressState() {
        this.$el.find('.controls').hide();
        this.ui.preloader.show();
    },
    _setCompleteState: function _setCompleteState() {
        this.$el.find('.controls').show();
        this.ui.preloader.hide();
    }
}); // FileUploadView
// ---------

exports.default = FileUploadView;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("resumablejs");

/***/ })
/******/ ]);
});
//# sourceMappingURL=fileUploadBox.view.js.map