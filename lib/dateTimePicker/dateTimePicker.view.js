(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("flatpickr"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "flatpickr"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("flatpickr")) : factory(root["backbone.marionette"], root["flatpickr"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_10__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("flatpickr");

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _flatpickr = __webpack_require__(10);

var _flatpickr2 = _interopRequireDefault(_flatpickr);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// DateTimePickerView
// ---------

var DateTimePickerView = _backbone2.default.ItemView.extend({
    tagName: 'input',
    className: 'date-picker flatpickr-date',
    locale_ru: {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        firstDayOfWeek: 1
    },
    flatpickr: false,
    flatpickrOptions: {
        dateFormat: 'Y.m.d',
        clickOpens: true
    },
    momentOptions: {
        dateFormat: 'YYYY.MM.DD'
    },
    template: false,
    initialize: function initialize() {
        // Localization
        _.extend(this.flatpickrOptions, {
            locale: {
                weekdays: {
                    longhand: this.locale_ru.days,
                    shorthand: this.locale_ru.daysShort
                },
                months: {
                    longhand: this.locale_ru.months,
                    shorthand: this.locale_ru.monthsShort
                },
                firstDayOfWeek: this.locale_ru.firstDayOfWeek
            }
        });

        if (this.getOption('flatpickrOptions')) {
            _.extend(this.flatpickrOptions, this.getOption('flatpickrOptions'));
        }
    },
    onAttach: function onAttach() {
        var that = this;
        this.flatpickr = new _flatpickr2.default("#" + that.id, that.flatpickrOptions);
    },
    onBeforeDestroy: function onBeforeDestroy() {
        this.flatpickr.clear();
        this.flatpickr.destroy();
        this.flatpickr = false;
    }
});

exports.default = DateTimePickerView;

/***/ })

/******/ });
});
//# sourceMappingURL=dateTimePicker.view.js.map