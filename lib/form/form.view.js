(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("qtip2"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "qtip2"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("qtip2")) : factory(root["backbone.marionette"], root["qtip2"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("qtip2");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _qtip = __webpack_require__(1);

var _qtip2 = _interopRequireDefault(_qtip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// FormView
// ---------

var FormView = _backbone2.default.LayoutView.extend({
    tagName: 'form',
    className: 'app-form',
    events: {},
    formEvents: {
        'change input': '_fieldChanged',
        'change select.select2-select': '_fieldChanged',
        'keypress': '_onPressEnter'
    },
    triggers: {
        'click .btn.submit': {
            event: 'form:submit',
            preventDefault: true
        }
    },
    fields: {},
    _errorClass: 'has-error',
    _dataValidationErrorCode: [422],
    _serverErrorCode: [500],
    constructor: function constructor(options) {
        _.extend(this.events, this.formEvents);

        _backbone2.default.LayoutView.prototype.constructor.call(this, options);
        this._formInitialize.apply(this, arguments);
    },
    _formInitialize: function _formInitialize() {
        var that = this;

        this.on('form:submit', this.onSubmit, this);
        this.on('validation:success', this.onFormValid, this);

        this.listenTo(this.model, 'save:success', this.onSaveSuccess);
        this.listenTo(this.model, 'save:error', this.onSaveError);

        Backbone.Validation.bind(this, {
            invalid: that._invalidFieldCallback,
            valid: that._validFieldCallback
        });
    },
    _invalidFieldCallback: function _invalidFieldCallback(view, attr, error) {
        var that = this;

        var $field = view.$el.find('#' + attr);
        if ($field.hasClass('select2-select')) {
            $field = $field.next('.select2-container');
        }
        if (!$field.hasClass(view._errorClass)) {
            $field.addClass(view._errorClass);
        }
        $($field).qtip({
            attr: 'data-tooltip',
            style: {
                classes: 'qtip-gmt',
                tip: {
                    border: 0,
                    width: 1,
                    height: 1
                }
            },
            position: {
                my: 'center left',
                at: 'center right',
                container: $($field).parent('div'),
                distance: 8
            },
            content: {
                text: error
            },
            hide: {
                event: 'click'
            }
        });
        $($field).qtip('api').show();
    },
    _validFieldCallback: function _validFieldCallback(view, attr, selector) {
        var that = this;
        var $field = view.$el.find('#' + attr);

        if ($field.hasClass('select2-select')) {
            $field = $field.next('.select2-container');
        }

        if ($field.hasClass(view._errorClass)) {
            $field.removeClass(view._errorClass);
            var qtipApi = $($field).qtip('api');
            qtipApi.destroy();
        }
    },
    _fieldChanged: function _fieldChanged(event) {
        var $input = $(event.currentTarget);
        var attrName = $input.attr('id');

        var fieldsValues = _.result(this, 'fields');

        this.model.set(attrName, fieldsValues[attrName]);
        var valid = this.model.isValid(attrName);
    },
    _onPressEnter: function _onPressEnter(event) {
        if (event.which !== 13) return;
        event.preventDefault();
        this.trigger('form:submit');
    },
    onSubmit: function onSubmit(args) {
        var fieldsValues = {};

        fieldsValues = _.result(this, 'fields');

        this.model.set(fieldsValues);
        this.model.validate();

        if (this.model.isValid()) {
            this.trigger('validation:success');
        } else {
            this.model.clear({ silent: true });
        }
    },
    onFormValid: function onFormValid() {
        this.model.save();
    },
    onSaveError: function onSaveError(model, response, opt) {
        var that = this;
        if (_.contains(this._dataValidationErrorCode, response.status)) {
            that.onDataValidationError(model, response, opt);
        } else if (_.contains(this._serverErrorCode, response.status)) {
            that.onServerError(model, response, opt);
        } else if (response.status == 404) {
            that.onNotFoundError(model, response, opt);
        } else {
            that.onError(model, response, opt);
        }
    },
    onDataValidationError: function onDataValidationError(model, response, opt) {
        var that = this;

        var invalidAttrs = {};
        _.each(response.responseJSON, function (errorObj, index) {
            var field = errorObj.field;
            var errorMessage = errorObj.message;

            invalidAttrs[field] = errorMessage;
            that._invalidFieldCallback(that, field, errorMessage);
        });
    },
    onError: function onError(model, response, opt) {
        if (_.isUndefined(response.responseJSON.error_description)) {
            App.errorNotify(App.t('global.unknown_error'));
        } else {
            App.errorNotify(response.responseJSON.error_description);
        }
    },
    onServerError: function onServerError(model, response, opt) {
        if (_.isUndefined(response.responseJSON.error_description)) {
            App.errorNotify(App.t('global.unknown_server_error'));
        } else {
            App.errorNotify(response.responseJSON.error_description);
        }
    },
    onNotFoundError: function onNotFoundError() {},
    onSaveSuccess: function onSaveSuccess(model, response, opt) {},
    onBeforeDestroy: function onBeforeDestroy() {
        // TODO: fix getting field names without invoking 'fields' function because the fields' values might not be 'gettable' at the time
        // this._clearQtip();
        Backbone.Validation.unbind(this);
        this.model.clear({ silent: true });
        this.model = false;
    },
    _clearQtip: function _clearQtip() {
        var that = this;

        var fieldsValues = _.result(this, 'fields');

        _.each(fieldsValues, function (field, index) {
            var $field = that.$el.find('#' + index);

            if ($field.length > 0) {
                if ($field.hasClass('select2-select')) {
                    $field = $field.next('.select2-container');
                }

                if (!_.isUndefined($($field).qtip('api'))) {
                    $($field).qtip('api').destroy();
                }
            }
        });
    }
});

exports.default = FormView;

/***/ })
/******/ ]);
});
//# sourceMappingURL=form.view.js.map